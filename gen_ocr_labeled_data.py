import os
import numpy as np
from PIL import Image, ImageFont, ImageDraw
import random
import cv2
import glob
import time
from tqdm import tqdm
from functools import partial

import re
import multiprocessing
from config import FONT_CONFIG, ID_CONFIG, settings as SETTINGS
from add_text import blend_text, add_noise_around, adjust_jpeg_compression

text_to_write = open(SETTINGS.text_to_write, 'r').readlines()   ### newsgroup
# text_to_write = ['TĂNG']
# text_to_write_length = len(text_to_write)

if not os.path.exists(SETTINGS.save_path):
    os.makedirs(SETTINGS.save_path)
if not os.path.exists(os.path.join(SETTINGS.save_path, 'img')):
    os.makedirs(os.path.join(SETTINGS.save_path, 'img'))
f_write = open(os.path.join(SETTINGS.save_path, "label.txt"),"w", buffering=1)

def check_collision_again_and_again(collision_mask_binary, label):
    img_h, img_w = collision_mask_binary.shape
    can_pasted = False
    for i_retry in range(SETTINGS.retry_time):
        # random
        font_name = random.choice(ID_CONFIG[cmt_type]['fonts'])
        # text bold in cccd -> font arial bold
        if font_name == 'arial' and label.isupper():
            if random.uniform(0,1) < 0.6:
                font_name = 'arial_bold'
        
        # Choose random font
        font_path = FONT_CONFIG[font_name]['path']
        font_size = random.choice(list(range(40, 60)))  #random.choice(list(range(48, 64)))
        font = ImageFont.truetype(font_path, font_size)

        # Choose position to put text
        range_x = range(int(ID_CONFIG[cmt_type]['limit_x'][0] * img_w), int(ID_CONFIG[cmt_type]['limit_x'][1] * img_w))
        range_y = range(int(ID_CONFIG[cmt_type]['limit_y'][0] * img_h), int(ID_CONFIG[cmt_type]['limit_y'][1] * img_h))
        x = random.choice(list(range_x))
        y = random.choice(list(range_y))
        
        (width, baseline), (offset_x, offset_y) = font.font.getsize(label)
        PAD_TOP = 4
        text_w = width - offset_x - 1
        text_h = baseline + PAD_TOP

        # Create text slice mask and draw text into it
        word_box_mask = Image.new('RGBA', (text_w, text_h), (0, 0, 0, 0))
        draw = ImageDraw.Draw(word_box_mask)
        draw.text((0-offset_x,0-offset_y+PAD_TOP/2), label, fill="white", font=font)

        # Create a blank mask
        text_mask_blank = Image.fromarray(np.zeros((img_h, img_w, 3)), mode='RGB')   # black blank image
        # paste slice mask to full blank image mask
        text_collision_mask = text_mask_blank.copy()
        text_collision_mask.paste(word_box_mask, (x, y), word_box_mask)
        text_collision_mask_binary = np.array(text_collision_mask)[:,:,0] / 255

        # check for collision
        is_out_of_frame = x + text_w > text_collision_mask.size[0] or y + text_h > text_collision_mask.size[1]
        intersection = np.logical_and(collision_mask_binary, text_collision_mask_binary)
        if not np.any(intersection) and not is_out_of_frame:
            can_pasted = True
            break

    if can_pasted == False:
        # continue
        return None, None, None, None
    else:
        return word_box_mask, font_name, (x, y), (text_w, text_h)

def gen_image_by_collision_path(i_collision):
    collision_path = collision_paths[i_collision]
    img_name = collision_path.split('/')[-1]
    img_path = os.path.join(processed_img_folder, img_name)
    cmt = cv2.imread(img_path)

    print('Process image {}'.format(img_path))

    collision_mask = cv2.imread(collision_path, 0)
    # Threshold
    ret,collision_mask = cv2.threshold(collision_mask,100,255,cv2.THRESH_BINARY)
    collision_mask_binary = collision_mask / 255

    for i_retry in range(SETTINGS.num_retry_per_image):
        img_h, img_w = collision_mask_binary.shape
        i_save_index =  i_collision * SETTINGS.num_retry_per_image + i_retry
        label = random.choice(text_to_write).strip() # text_to_write[i_save_index % text_to_write_length].strip() 

        # filter out unwanted characters
        # out_of_char = f'[^{SETTINGS.characters}]'
        # if re.search(out_of_char, label):
        #     continue

        word_box_mask, font_name, word_pos, word_box_shape = check_collision_again_and_again(collision_mask_binary, label)
        if word_box_mask == None:
            return
        (x, y) = word_pos
        (text_w, text_h) = word_box_shape
        color = [random.randint(0,35), random.randint(0,35), random.randint(0,35)]
        
        # augment text mask
        word_box_mask = np.array(word_box_mask)
        text_augmenter = FONT_CONFIG[font_name]['text_augment']
        word_box_mask = text_augmenter(images=[word_box_mask])[0]

        full_background = cmt.copy()
        word_box_image_rgba = blend_text(full_background, word_box_mask, color, word_pos, word_box_shape, i_save_index)
        num_morphology = int(text_h / 30)
        word_box_image, new_alpha = add_noise_around(word_box_image_rgba, num_morphology, gaussian_noise=True)
        word_box_image = adjust_jpeg_compression(word_box_image, quality=random.randint(50,75))
        # word_box_image = word_box_image_rgba[:,:,:3]

        # paste slice mask to orginal image
        xmin, ymin, xmax, ymax = (x, y, x+text_w, y+text_h)
        image_with_text = full_background.copy()
        image_with_text[ymin:ymax, xmin:xmax] = word_box_image

        # cut text
        CUT_PAD_X_M = np.random.choice(np.arange(0, 10)) * 0.01
        CUT_PAD_X_P = np.random.choice(np.arange(0, 10)) * 0.01
        CUT_PAD_Y_M = np.random.choice(np.arange(5, 15)) * 0.01
        CUT_PAD_Y_P = np.random.choice(np.arange(5, 15)) * 0.01

        xmin = np.clip(int(xmin-CUT_PAD_X_M*text_w), 0, img_w)
        ymin = np.clip(int(ymin-CUT_PAD_Y_M*text_h), 0, img_h)
        xmax = np.clip(int(xmax+CUT_PAD_X_P*text_w), 0, img_w)
        ymax = np.clip(int(ymax+CUT_PAD_Y_P*text_h), 0, img_h)

        # full image augmentation
        image_augmenter = FONT_CONFIG[font_name]['image_augment']
        image_with_text = image_augmenter(images=[image_with_text])[0]
        word_box_image = image_with_text[ymin:ymax, xmin:xmax, :]

        path_to_save = '{}/{}_{:08d}.png'.format('img', cmt_type, i_save_index)
        print('LABEL: {}, from {}'.format(label, path_to_save))
        # word_slice.save(os.path.join(SETTINGS.save_path, path_to_save))
        cv2.imwrite(os.path.join(SETTINGS.save_path, path_to_save), word_box_image)
        line_to_write = '{}\t{}\n'.format(path_to_save, label.replace('-', ''))
        f_write.write(line_to_write)
        
start_time = time.time()
for cmt_type in list(ID_CONFIG.keys()):
    print('\nProcess image for type {}'.format(cmt_type))
    collision_folder = os.path.join(SETTINGS.src_path, SETTINGS.collision_set, cmt_type)
    processed_img_folder = os.path.join(SETTINGS.src_path, 'processed', cmt_type)

    collision_paths = glob.glob(os.path.join(collision_folder, '*.png'))
    collision_paths.extend(glob.glob(os.path.join(collision_folder, '*.jpg')))

    # multithreading
    with multiprocessing.Pool(processes = SETTINGS.num_worker) as pool:
        pool.map(gen_image_by_collision_path, range(len(collision_paths)))

    '''
    for i_collision in range(len(collision_paths)):
        collision_path = collision_paths[i_collision]
        img_name = collision_path.split('/')[-1]
        img_path = os.path.join(processed_img_folder, img_name)
        img = cv2.imread(img_path)

        print('Process image {}'.format(img_path))

        collision_mask = cv2.imread(collision_path, 0)
        # Threshold
        ret,collision_mask = cv2.threshold(collision_mask,100,255,cv2.THRESH_BINARY)
        collision_mask_binary = collision_mask / 255
        
        #Create a blank mask
        text_mask_blank = Image.fromarray(np.zeros_like(np.array(img.copy()), np.uint8))    # black blank image
    
        # multithreading
        with multiprocessing.Pool(processes = SETTINGS.num_worker) as pool:
            func = partial(gen_image_per_retry, collision_mask_binary, text_mask_blank)
            pool.map(func, range(SETTINGS.num_retry_per_image))
        '''

f_write.close()
print('TIME ELAPSED: ', time.strftime('%H:%M:%S', time.gmtime(time.time() - start_time)))