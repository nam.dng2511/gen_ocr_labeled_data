import anyconfig
import munch
import os
import random
from augmentation import text_augmenter_pala, text_augmenter_arial, text_augmenter_trixi, image_augmenter_pala, image_augmenter_arial, image_augmenter_trixi

settings = anyconfig.load('settings.yaml')
settings = munch.munchify(settings)

ID_CONFIG = {
    # 'cccd_back_transform': {
    #     'limit_x': [0.3, 1],
    #     'limit_y': [0.25, 0.65],
    #     'fonts': ['arial']
    # },
    # 'cccd_front_transform': {
    #     'limit_x': [0.3, 1],
    #     'limit_y': [0.01, 1],
    #     'fonts': ['arial']
    # },
    # 'cmndc_back_transform': {
    #     'limit_x': [0.4, 1],
    #     'limit_y': [0.01, 0.6],
    #     'fonts': ['trixi_regular', 'trixi_tex', 'pala']
    # },
    'cmndc_front_transform': {
        'limit_x': [0.3, 1],
        'limit_y': [0.01, 1],
        'fonts': ['trixi_regular', 'trixi_tex', 'pala']
    }
}

FONT_CONFIG = {
    'arial': {
        'path': os.path.join(settings.font_path, 'font_tnr_arial/arial.ttf'),
        'text_augment': text_augmenter_arial,
        'image_augment': image_augmenter_arial,
        'degrade_ratio': random.uniform(1, 2)
    },
    'arial_bold': {
        'path': os.path.join(settings.font_path, 'font_tnr_arial/arialbd.ttf'),
        'text_augment': text_augmenter_arial,
        'image_augment': image_augmenter_arial,
        'degrade_ratio': random.uniform(1, 2)
    },
    'trixi_regular': {
        'path':  os.path.join(settings.font_path, 'TrixiPro font/Trixi Pro Regular.ttf'),
        'text_augment': text_augmenter_trixi,
        'image_augment': image_augmenter_trixi,
        'degrade_ratio': random.uniform(1, 2)
    },
    'trixi_tex': {
        'path': os.path.join(settings.font_path, 'TrixiPro font/Trixi Pro Tex.ttf'),
        'text_augment': text_augmenter_trixi,
        'image_augment': image_augmenter_trixi,
        'degrade_ratio': random.uniform(1, 2)
    },
    'pala': {
        'path': os.path.join(settings.font_path, 'pala.ttf'),
        'text_augment': text_augmenter_pala,
        'image_augment': image_augmenter_pala,
        'degrade_ratio': random.uniform(1.2, 2.5)
    }
}