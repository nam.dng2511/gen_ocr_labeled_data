from collections import Counter

CHARACTERS = "0123456789aáàãạảăắằẵặẳâấầẫậẩbcdđeéèẽẹẻêếềễệểfghiíìĩịỉjklmnoóòõọỏôốồỗộổơớờỡợởpqrstuúùũụủưứừữựửvwxyýỳỹỵỷzAÁÀÃẠẢĂẮẰẴẶẲÂẤẦẪẬẨBCDĐEÉÈẼẸẺÊẾỀỄỆỂFGHIÍÌĨỊỈJKLMNOÓÒÕỌỎÔỐỒỖỘỔƠỚỜỠỢỞPQRSTUÚÙŨỤỦƯỨỪỮỰỬVWXYÝỲỸỴỶZ\/?.,:;()!'/-"

lines = open('/home/namdng/Documents/gen_ocr_labeled_data/newsgroup/momo_23_04.txt', 'r').readlines()

def calculate_stat(all_words):
    stats = {}
    for c in list(CHARACTERS):
        stats[c] = 0

    for word in all_words:
        counter = Counter(word)
        for key in counter.keys():
            if key in stats.keys():
                stats[key] += counter[key]

    return stats

print(calculate_stat(lines))
