import cv2

def adjust_jpeg_compression(bgr, quality=5):
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), quality]
    ret, encimg = cv2.imencode('.jpg', bgr, encode_param)
    decimg = cv2.imdecode(encimg, 1)
    return decimg