from imgaug import augmenters as iaa

text_augmenter_pala = iaa.Sequential([
    iaa.GaussianBlur(sigma=(0, 1)),
    # iaa.Sometimes(0.1, iaa.MotionBlur(k=(3,6), angle=[-180, -90, 0, 90, 180])),
    iaa.Sometimes(0.1, iaa.ShearX((-5, 5)))
])
text_augmenter_trixi = iaa.Sequential([
    iaa.GaussianBlur(sigma=(0, 1)),
    # iaa.Sometimes(0.1, iaa.MotionBlur(k=(3,6), angle=[-180, -90, 0, 90, 180])),
    iaa.Sometimes(0.1, iaa.ShearX((-5, 5)))
])
text_augmenter_arial = iaa.Sequential([
    iaa.GaussianBlur(sigma=(0, 1)),
    # iaa.Sometimes(0.1, iaa.MotionBlur(k=(3,6), angle=[-180, -90, 0, 90, 180])),
    iaa.Sometimes(0.2, iaa.ShearX((-10, 10)))
])
image_augmenter_pala = iaa.Sequential([
    iaa.AddToBrightness((0, 80)),
])
image_augmenter_trixi = iaa.Sequential([
    iaa.AddToBrightness((0, 60)),
])
image_augmenter_arial = iaa.Sequential([
    iaa.AddToBrightness((0, 100)),
])
