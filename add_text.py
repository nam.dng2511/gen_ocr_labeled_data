from PIL import Image
import numpy as np
import random
import cv2
import os

def blend_text(full_background, word_box_mask, color, word_pos, word_box_shape, i_save_index):
    '''
    full_background: RGB image (numpy array)
    word_box_mask: RGBA word box mask of text (numpy array)
    color: text color
    word_pos: top left position
    word_box_shape: word box width and height

    Return: 
    '''
    (x, y) = word_pos
    (text_w, text_h) = word_box_shape
    alpha_mask = word_box_mask[:,:,0] > 0

    # prepare background (area to put text on), mask and foreground_color color
    xmin, ymin, xmax, ymax = (x, y, x+text_w, y+text_h)
    background = full_background[ymin:ymax, xmin:xmax, :]
    word_box_mask = word_box_mask[:,:,:3]
    foreground_color = np.ones((text_h, text_w, 3), np.uint8) * np.array(color).reshape(1, 1, 3)
    foreground_color = foreground_color.astype(np.uint8)


    ratio = np.expand_dims(np.max(word_box_mask, axis=2), axis=2) / 255
    # cv2.imwrite('/home/namdng/Documents/gen_ocr_labeled_data/debug/{}.png'.format(str(random.randint(0, 100))), ratio*255)
    # add noise to stroke
    # noise = False
    # if noise is True:
    #     small_bg_h, small_bg_w = int(text_h / 3), int(text_w / 3)
    #     std = 0.2
    #     noise = np.random.normal(1, std, (small_bg_h, small_bg_w, 1))
    #     noise = cv2.resize(noise, (text_w, text_h), interpolation=cv2.INTER_CUBIC)
    #     noise = np.expand_dims(noise, axis=-1)
    #     noise[noise>1] = 1
    #     ratio = ratio * noise

    '''
    hsv_bg = cv2.cvtColor(background, cv2.COLOR_BGR2HSV)
    should_save_high_quality = False

    v_copy = np.ones_like(hsv_bg[:,:,2]) * 255
    v_copy[alpha_mask] = hsv_bg[:,:,2][alpha_mask].copy()

    # check the similar between bg and fg
    if np.min(v_copy) > 75: # (75,75,75) => HSV, V channel
        hsv_fg = cv2.cvtColor(foreground_color, cv2.COLOR_BGR2HSV)

        # Check color of text and color of background are similar
        delta_hue = 36

        diff_h = np.fabs(hsv_fg[:,:,0].astype(np.float) - hsv_bg[:,:,0].astype(np.float))
        hue_mask = np.bitwise_and(np.bitwise_or(diff_h <= delta_hue, diff_h >= 180.0 - delta_hue), alpha_mask)

        rechose_color = (np.random.randint(0, 35), np.random.randint(0, 35), np.random.randint(0, 35))
        foreground_color[hue_mask] = rechose_color

    # Check dark text color and dark background case
    # Only process for background containing dark area
    else:
        # should_save_high_quality = True
        hsv_fg = cv2.cvtColor(foreground_color, cv2.COLOR_BGR2HSV)
        dark_hue_fg_mask = np.bitwise_and(hsv_fg[:,:,2] <= 75, alpha_mask)

        rechose_color = (np.random.randint(200, 255), np.random.randint(200, 255), np.random.randint(200, 255))
        # print('should_save_high_quality: ', i_save_index, rechose_color)

        diff_v = np.fabs(hsv_fg[:,:,2].astype(np.float) - hsv_bg[:,:,2].astype(np.float))

        value_mask = np.bitwise_and(diff_v <= 39, dark_hue_fg_mask)
        value_mask[np.bitwise_and(v_copy <= 125, alpha_mask)] = 1

        foreground_color[value_mask] = rechose_color
        # cv2.imwrite(os.path.join('/home/namdng/Documents/gen_ocr_labeled_data/debug', '{}.jpg'.format(int(i_save_index))), foreground_color)
    '''

    # blend image
    word_box_image = background * (1-ratio) + foreground_color * ratio
    ratio[ratio>0] = 1

    # stack alpha to the last dimension
    big_ratio = np.zeros((text_h, text_w, 1), np.uint8)#*255
    big_ratio[:, :, :] = ratio
    word_box_image = np.concatenate((word_box_image, big_ratio*255), axis=2).astype(np.uint8) #create RGBA image

    return word_box_image

def add_noise_around(bgra, num_morphology, gaussian_noise):
    alpha = bgra[:, :, 3]

    kernel = np.ones((3, 3), dtype=np.uint8)
    alpha_dilate = cv2.dilate(alpha, kernel, iterations=num_morphology)
    alpha_diff = alpha_dilate - alpha
    alpha_diff[alpha_diff!=0] = 1

    bgr = bgra[:, :, :3]

    if gaussian_noise is True:
        # change the value of pixels
        bg_h, bg_w = bgr.shape[:2]
        small_bg_h, small_bg_w = int(bg_h / 3), int(bg_w / 3)

        std = 0.2
        noise = np.random.normal(1, std, (small_bg_h, small_bg_w, 1))

        noise = cv2.resize(noise, (bg_w, bg_h), interpolation=cv2.INTER_CUBIC)
        noise = np.expand_dims(noise, axis=-1)

        noise[noise<1.0 - std] = 1.0 - std
        noise[noise>1.0 + std] = 1.0 + std
        bgr[alpha_diff!=0] = np.clip(bgr[alpha_diff!=0]*noise[alpha_diff!=0], 0, 255)
    
    else:
        # using Gaussian blur
        blur = cv2.GaussianBlur(bgr,(5,5),0)
        bgr[alpha_diff!=0] = blur[alpha_diff!=0].copy()

    return bgr, alpha_dilate

def adjust_jpeg_compression(bgr, quality=5):
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), quality]
    ret, encimg = cv2.imencode('.jpg', bgr, encode_param)
    decimg = cv2.imdecode(encimg, 1)
    return decimg