from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

label = 'DẨN'
# font = ImageFont.truetype('/home/namdng/Documents/gen_ocr_labeled_data/fonts/pala.ttf', 64)
font = ImageFont.truetype('/home/namdng/Documents/gen_ocr_labeled_data/fonts/font_tnr_arial/arial.ttf', 64)

# text_w, text_h = font.getsize(label)
(width, baseline), (offset_x, offset_y) = font.font.getsize(label)
print((width, baseline), (offset_x, offset_y))
text_w = width - 1 - offset_x
text_h = baseline + 2
# print(font.getmask(label).getbbox())
text_slice_mask = Image.new('RGB', (text_w, text_h), (0, 0, 0))

draw = ImageDraw.Draw(text_slice_mask)
draw.text((-offset_x,-offset_y), label, (255, 255, 255), font=font)
# draw.text((0,0), label, (255, 255, 255), font=font)
# draw horizontally
# draw.line([(0, -offset_y), (1000, -offset_y)])
# draw vertically
# draw.line([(-offset_x, 0), (-offset_x, 1000)])

text_slice_mask.save('1.jpg')

# ascent, descent = font.getmetrics()

# x, y, w, h = font.getmask(text).getbbox()

# print('offset_y: ', offset_y)
# print('baseline: ', baseline)

# print('get mask size: ', w, h)
